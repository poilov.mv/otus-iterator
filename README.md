[![pipeline status](https://gitlab.com/poilov.mv/otus-iterator/badges/master/pipeline.svg)](https://gitlab.com/poilov.mv/otus-iterator/-/commits/master)
[![coverage report](https://gitlab.com/poilov.mv/otus-iterator/badges/master/coverage.svg)](https://gitlab.com/poilov.mv/otus-iterator/-/commits/master)

# Шаблоны проектирования

## Домашнее задание по теме **Итератор**
**Цель**: Получите навыки применения шаблона "итератор" и знания формировании чисел Фибонначи
1. Создать программу, которая генерит числа фибонначи в указанном диапазоне в указанный файл. Предусмотреть возможность движения в обратном направлении (например, с использованием формулы Binet)
2. Реализовать в программе абстрактную фабрику и конкретные фабрики, отвечающие за каждый вариант сортировки как продукты.
3. Программа записывает результаты в выходной файл данных.
4. Если потребуется использовать Итератор в проектной работе, предоставить описание в текстовом файле в GitHub репозитории где конкретно и в какой роли используется этот шаблон.
5. нарисовать диаграмму классов.

## Диаграмма классов
### Наследование
```mermaid
classDiagram
    class FibonachiRange {
      list<uint64> range

      +FibonachiRange(from, to);
      +begin() Iterator
      +end() Iterator

      +toVector() list<uint64>
      -fromRange(uint64_t from, uint64_t to) Container
    }

    class Iterator {
      -container : FibonachiRange
      -index : int

      +Iterator(container, startIndex)

      +hasNext() bool
      +next() uint64

      +hasPrev() bool
      +prev() uint64
    }

    FibonachiRange --> Iterator : использует

```
### Вспомогательные классы
```mermaid
classDiagram
  class ArgumentParser {
    list<string> args
    +isArgsValid() bool
    +operation() string
    +sourceFilePath() string
    +targetFilePath() string
    +synopsis() string
  }

  class TextFile {
    string filepath
    string content

    +TextFile(filepath)
    +path() string
    +setContent(value)
    +getContent()
    +read() bool
    +write() bool
    +exists() bool
    +remove()
    +writeContent(content, filepath) static
  }
```

## Исходные файлы
### Основное приложение
* source/main.cpp
* source/main_app.h
* source/main_app.cpp
### Реализация операций с диапазоном чисел Фибоначчи
* source/FibonachiRange.h
* source/FibonachiRange.cpp
### Класс разбора аргументов командной строки
* source/ArgumentParser.h
* source/ArgumentParser.cpp
### Класс обертка для чтения и сохранения текстового файла
* source/TextFile.h
* source/TextFile.cpp
### Преобразование к выходному формату файла
* source/OutputFormat.h
* source/OutputFormat.cpp
### Тесты
* unit_tests/test_argumentparser.cpp
* unit_tests/test_outputformat.cpp
* unit_tests/test_textfile.cpp
* unit_tests/test_main.cpp
* unit_tests/test_fibonachirange.cpp
