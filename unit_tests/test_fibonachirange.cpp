#include "gtest/gtest.h"

#include "FibonachiRange.h"


TEST(TestFibonachiRange, can_get_right_fibonaci_numbers) {
    auto range = FibonachiRange(7, 30);

    std::vector<uint64_t> expected = { 8, 13, 21 };
    ASSERT_EQ(expected, range.toVector());
}

TEST(TestFibonachiRange, can_get_fibonaci_numbers_from_zero) {
    auto range = FibonachiRange(0, 10);

    std::vector<uint64_t> expected = { 0, 1, 1, 2, 3, 5, 8 };
    ASSERT_EQ(expected, range.toVector());
}

TEST(TestFibonachiRange, valid_begin_iterator_has_next) {
    auto range = FibonachiRange(7, 30);

    auto it = range.begin();

    ASSERT_TRUE(it.hasNext());
}

TEST(TestFibonachiRange, begin_iterator_starts_with_right_number) {
    auto range = FibonachiRange(7, 30);

    auto it = range.begin();

    ASSERT_EQ(8, it.next());
}

TEST(TestFibonachiRange, begin_iterator_gives_right_next_number) {
    auto range = FibonachiRange(7, 30);

    auto it = range.begin();
    it.next();

    ASSERT_EQ(13, it.next());
}

TEST(TestFibonachiRange, begin_iterator_has_no_next_in_the_end) {
    auto range = FibonachiRange(7, 9);

    auto it = range.begin();
    it.next();

    ASSERT_FALSE(it.hasNext());
}

TEST(TestFibonachiRange, valid_end_iterator_has_prev) {
    auto range = FibonachiRange(7, 30);

    auto it = range.end();

    ASSERT_TRUE(it.hasPrev());
}

TEST(TestFibonachiRange, end_iterator_starts_with_right_number) {
    auto range = FibonachiRange(7, 30);

    auto it = range.end();

    ASSERT_EQ(21, it.prev());
}

TEST(TestFibonachiRange, end_iterator_gives_right_prev_number) {
    auto range = FibonachiRange(7, 30);

    auto it = range.end();
    it.prev();

    ASSERT_EQ(13, it.prev());
}

TEST(TestFibonachiRange, reverse_iterator_has_no_prev_in_the_end) {
    auto range = FibonachiRange(7, 9);

    auto it = range.end();
    it.prev();

    ASSERT_FALSE(it.hasPrev());
}
