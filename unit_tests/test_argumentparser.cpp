#include "gtest/gtest.h"

#include "ArgumentParser.h"


class TestArgumentParser : public ::testing::Test {
protected:
    ArgumentParser createParser(
        const std::string &arg1 = std::string(),
        const std::string &arg2 = std::string(),
        const std::string &arg3 = std::string(),
        const std::string &arg4 = std::string()
    ) {
        std::string p("program");
        std::string a1(arg1);
        std::string a2(arg2);
        std::string a3(arg3);
        std::string a4(arg4);

        int count = 1
            + (!a1.empty() ? 1 : 0)
            + (!a2.empty() ? 1 : 0)
            + (!a3.empty() ? 1 : 0)
            + (!a4.empty() ? 1 : 0);

        char *argv[] = { &p.at(0),
            a1.empty() ? NULL : &a1.at(0),
            a2.empty() ? NULL : &a2.at(0),
            a3.empty() ? NULL : &a3.at(0),
            a4.empty() ? NULL : &a4.at(0) };

        return ArgumentParser(count, argv);
    }
};

TEST_F(TestArgumentParser, can_parse_three_args) {
    auto parser = createParser("5", "20", "target");

    ASSERT_TRUE(parser.isArgsValid());
}

TEST_F(TestArgumentParser, can_parse_four_args) {
    auto parser = createParser("5", "20", "target", "desc");

    ASSERT_TRUE(parser.isArgsValid());
}

TEST_F(TestArgumentParser, cant_parse_two_args) {
    auto parser = createParser("5", "20");

    ASSERT_FALSE(parser.isArgsValid());
}

TEST_F(TestArgumentParser, can_get_range_from) {
    auto parser = createParser("7", "10", "target");

    ASSERT_EQ(7, parser.rangeFrom());
}

TEST_F(TestArgumentParser, returns_zero_if_wrong_range_from_format) {
    auto parser = createParser("d", "10", "target");

    ASSERT_EQ(0, parser.rangeFrom());
}

TEST_F(TestArgumentParser, can_get_range_to) {
    auto parser = createParser("7", "10", "target");

    ASSERT_EQ(10, parser.rangeTo());
}

TEST_F(TestArgumentParser, returns_zero_if_wrong_range_to_format) {
    auto parser = createParser("8", "f", "target");

    ASSERT_EQ(0, parser.rangeTo());
}

TEST_F(TestArgumentParser, can_get_target_file) {
    auto parser = createParser("7", "10", "target");

    ASSERT_EQ(std::string("target"), parser.targetFilePath());
}

TEST_F(TestArgumentParser, can_get_default_sort_order) {
    auto parser = createParser("7", "10", "target");

    ASSERT_TRUE(parser.sortAscending());
}

TEST_F(TestArgumentParser, can_set_descending_sort_order) {
    auto parser = createParser("7", "10", "target", "desc");

    ASSERT_FALSE(parser.sortAscending());
}

TEST_F(TestArgumentParser, wrong_sort_order_uses_default) {
    auto parser = createParser("7", "10", "target", "any");

    ASSERT_TRUE(parser.sortAscending());
}

TEST_F(TestArgumentParser, can_get_synopsis) {
    auto parser = createParser();

    auto synopsis = parser.synopsis();

    ASSERT_FALSE(synopsis.empty());
}
