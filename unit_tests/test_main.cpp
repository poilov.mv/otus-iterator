#include "gtest/gtest.h"

#include "main_app.h"
#include "TextFile.h"


class TestMain : public ::testing::Test {
protected:
    TextFile targetFile;

    TestMain()
        : targetFile("target.txt") {
    }

    void TearDown() override {
        targetFile.remove();
    }

    int run_main(
        const std::string &rangeFrom = std::string(),
        const std::string &rangeTo = std::string(),
        const std::string &targetFilePath = std::string(),
        const std::string &sortOrder = std::string()
    ) {
        std::string pr("program");
        std::string rf(rangeFrom);
        std::string rt(rangeTo);
        std::string tp(targetFilePath);
        std::string so(sortOrder);

        int count = 1
            + (!rf.empty() ? 1 : 0)
            + (!rt.empty() ? 1 : 0)
            + (!tp.empty() ? 1 : 0)
            + (!so.empty() ? 1 : 0);

        char *argv[] = { &pr.at(0), 
            rt.empty() ? NULL : &rf.at(0),
            rf.empty() ? NULL : &rt.at(0), 
            tp.empty() ? NULL : &tp.at(0),
            so.empty() ? NULL : &so.at(0) };
        return main_app(count, argv);
    }
};


TEST_F(TestMain, can_generate_fibonachi_in_range_ascending) {
    ASSERT_EQ(0, run_main("4", "30", targetFile.path()));

    targetFile.read();
    auto resultContent = targetFile.getContent();
    auto expectedContent = "5 8 13 21";
    ASSERT_EQ(expectedContent, resultContent);
}

TEST_F(TestMain, can_generate_fibonachi_in_range_descending) {
    ASSERT_EQ(0, run_main("7", "50", targetFile.path(), "desc"));

    targetFile.read();
    auto resultContent = targetFile.getContent();
    auto expectedContent = "34 21 13 8";
    ASSERT_EQ(expectedContent, resultContent);
}

TEST_F(TestMain, show_synopsis_and_returns_error_without_args) {
    ASSERT_EQ(1, run_main());
}

TEST_F(TestMain, returns_error_with_wrong_target_file) {
    auto targetPath = "wrong_file_name_?*/.txt";
    ASSERT_EQ(2, run_main("8", "15", targetPath));
}

TEST_F(TestMain, can_get_sorted_ascending_range) {
    FibonachiRange range(4, 30);
    auto result = sortedNumbers(range, true);

    std::vector<uint64_t> expected = {5, 8, 13, 21};
    ASSERT_EQ(expected, result);
}

TEST_F(TestMain, can_get_sorted_descending_range) {
    FibonachiRange range(7, 50);
    auto result = sortedNumbers(range, false);

    std::vector<uint64_t> expected = { 34, 21, 13, 8 };
    ASSERT_EQ(expected, result);
}
