#include "gtest/gtest.h"

#include "OutputFormat.h"


TEST(TestOutputFormat, can_format_numbers_to_single_line) {
    auto result = toSingleLineString({1, 2, 3});

    ASSERT_EQ("1 2 3", result);
}
