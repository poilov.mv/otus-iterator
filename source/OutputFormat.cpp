#include "OutputFormat.h"

#include <sstream>


std::string toSingleLineString(const std::vector<uint64_t> &value) {
    std::stringstream ss;
    for (size_t i = 0; i < value.size(); ++i) {
        if (i != 0) {
            ss << " ";
        }
        ss << value[i];
    }
    return ss.str();
}
