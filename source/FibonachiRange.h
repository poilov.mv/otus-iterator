#pragma once

#include <vector>
#include <cstdint>


class FibonachiRange {
    typedef std::vector<uint64_t> Container;
    Container range;

public:
    class Iterator {
        const FibonachiRange *container;
        Container::size_type index;

    public:
        Iterator(const FibonachiRange *container, Container::size_type startIndex);

        bool hasNext() const;
        uint64_t next();

        bool hasPrev() const;
        uint64_t prev();
    };

    FibonachiRange(uint64_t from, uint64_t to);

    Iterator begin() const;
    Iterator end() const;

    std::vector<uint64_t> toVector() const;

private:
    static Container fromRange(uint64_t from, uint64_t to);
};
