#pragma once

#include <string>
#include <vector>

class ArgumentParser {
    std::vector<std::string> args;

public:
    ArgumentParser(int argc, char **argv);

    bool isArgsValid() const;

    int rangeFrom() const;
    int rangeTo() const;
    bool sortAscending() const;
    std::string targetFilePath() const;

    std::string synopsis() const;
};
