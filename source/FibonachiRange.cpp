#include "FibonachiRange.h"


FibonachiRange::Iterator::Iterator(const FibonachiRange *container, Container::size_type startIndex)
    : container(container)
    , index(startIndex) {}

bool FibonachiRange::Iterator::hasNext() const {
    return index < container->range.size();
}

uint64_t FibonachiRange::Iterator::next() {
    return container->range[index++];
}

bool FibonachiRange::Iterator::hasPrev() const {
    return 0 < index;
}

uint64_t FibonachiRange::Iterator::prev() {
    return container->range[--index];
}

FibonachiRange::FibonachiRange(uint64_t from, uint64_t to) {
    range = fromRange(from, to);
}

FibonachiRange::Iterator FibonachiRange::begin() const {
    return Iterator(this, 0);
}

FibonachiRange::Iterator FibonachiRange::end() const {
    return Iterator(this, range.size());
}

std::vector<uint64_t> FibonachiRange::toVector() const {
    return range;
}

FibonachiRange::Container FibonachiRange::fromRange(uint64_t from, uint64_t to) {
    Container result;

    uint64_t number = 0;
    uint64_t next = 1;
    while (number <= to) {
        if (from <= number && number <= to) {
            result.emplace_back(number);
        }
        std::swap(number, next);
        next = number + next;
    }

    return result;
}
