#include "ArgumentParser.h"

#include <sstream>


ArgumentParser::ArgumentParser(int argc, char **argv) {
    for (int i = 1; i < argc; ++i) {
        args.push_back(argv[i]);
    }
}

bool ArgumentParser::isArgsValid() const {
    return args.size() == 3 || args.size() == 4;
}

std::string ArgumentParser::synopsis() const {
    std::stringstream ss;
    ss << "Usage:" << std::endl;
    ss << "fibonachi.exe <range_from> <range_to> <output_file> [desc]" << std::endl;
    return ss.str();
}

int ArgumentParser::rangeFrom() const {
    return std::atoi(args[0].c_str());
}

int ArgumentParser::rangeTo() const {
    return std::atoi(args[1].c_str());
}

bool ArgumentParser::sortAscending() const {
    if (args.size() == 4) {
        return args[3] != "desc";
    }
    return true;
}

std::string ArgumentParser::targetFilePath() const {
    return args[2];
}
