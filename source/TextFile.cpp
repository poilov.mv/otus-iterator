#include "TextFile.h"

#include <fstream>
#include <sstream>


TextFile::TextFile(const std::string &filepath)
    : filepath(filepath) {}

std::string TextFile::getContent() const {
    return content;
}

void TextFile::setContent(const std::string &value) {
    content = value;
}

bool TextFile::exists() const {
    std::ifstream stream(filepath);
    return stream.is_open();
}

bool TextFile::write() {
    std::ofstream stream(filepath);
    if (!stream.is_open()) {
        return false;
    }

    stream << content;
    return true;
}

bool TextFile::read() {
    std::ifstream stream(filepath);
    if (!stream.is_open()) {
        return false;
    }

    content = std::string(std::istreambuf_iterator<char>(stream),
        std::istreambuf_iterator<char>());
    return true;
}

void TextFile::remove() {
    std::remove(filepath.c_str());
}

std::string TextFile::path() const {
    return filepath;
}

bool TextFile::writeContent(const std::string &content, const std::string &filePath) {
    TextFile file(filePath);
    file.setContent(content);
    return file.write();
}
