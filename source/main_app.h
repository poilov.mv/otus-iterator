#pragma once

#include "FibonachiRange.h"


std::vector<uint64_t> sortedNumbers(const FibonachiRange &range, bool sortAscending);

int main_app(int argc, char **argv);
