#include "main_app.h"

#include "ArgumentParser.h"
#include "OutputFormat.h"
#include "TextFile.h"

#include <iostream>


std::vector<uint64_t> sortedNumbers(const FibonachiRange &range, bool sortAscending) {
    std::vector<uint64_t> numbers;
    if (sortAscending) {
        auto it = range.begin();
        while (it.hasNext()) {
            numbers.emplace_back(it.next());
        }
    }
    else {
        auto it = range.end();
        while (it.hasPrev()) {
            numbers.emplace_back(it.prev());
        }
    }
    return numbers;
}

int main_app(int argc, char **argv) {
    auto args = ArgumentParser(argc, argv);
    if (args.isArgsValid()) {
        auto fibonachiRange = FibonachiRange(args.rangeFrom(), args.rangeTo());

        auto numbers = sortedNumbers(fibonachiRange, args.sortAscending());

        auto content = toSingleLineString(numbers);

        if (!TextFile::writeContent(content, args.targetFilePath())) {
            std::cout << "Can not write file: " << args.targetFilePath() << std::endl;
            return 2;
        }

        return 0;
    } else {
        std::cout << args.synopsis();
        return 1;
    }
}
